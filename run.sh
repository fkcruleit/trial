#!/bin/bash

cd "$(dirname "$0")" || exit
mkdir -p build
cd build || exit
cmake ..
make

if [ $? -eq 0 ]; then
    echo "Build successful. Running the project..."
    ./TPproject
else
    echo "Build failed. Please check the build output for errors."
fi
